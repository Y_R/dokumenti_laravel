<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[
    'uses' => 'Document@getDocuments',
    'as' => 'documents'
]);

Route::get('/addDocument',[
    'uses' => 'Document@getCategories',
    'as' => 'addDocument'
]);

Route::post('/newDocument',[
    'uses' => 'Document@addDocument',
    'as' => 'document.new'
]);

Route::post('/newDocumentCategory',[
    'uses' => 'Document@addDocumentCategory',
    'as' => 'document.newCategory'
]);

Route::get('/generate/dogovor_naem', function () {
    return view('generate.dogovor_naem');
})->name('generate.dogovor_naem');

Route::any('/generate/dogovor_naem',[
    'uses' => 'GenerateDocumentController@getRegion',
    'as' => 'region'
]);

Route::post('/generate/dogovor_naem/convert2txt',[
    'uses' => 'GenerateDocumentController@convert2txt',
    'as' => 'convert2txt'
]);


Route::post('/generate/dogovor_naem/ajax',[
    'uses' => 'GenerateDocumentController@getCity',
    'as' => 'ajax'
]);