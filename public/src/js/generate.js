
// Activate Next Step

$(document).ready(function() {
    $('#form_validate')

        .bootstrapValidator({
        framework: 'bootstrap',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            firstname: {
                validators: {
                    stringLength: {
                        min: 2,
                        message: 'Невалидно име!'
                    },
                    notEmpty: {
                        message: 'Това поле не може да бъде празно!'
                    }
                }
            },
        surename: {
            validators: {
                stringLength: {
                    min: 2,
                    message: 'Невалидно име!'
                },
                notEmpty: {
                    message: 'Това поле не може да бъде празно!'
                }
            }
        },
        lastname: {
            validators: {
                stringLength: {
                    min: 2,
                    message: 'Невалидно име!'
                },
                notEmpty: {
                    message: 'Това поле не може да бъде празно!'
                }
            }
        },
        egn: {
            validators: {
                id: {
                    country: 'BG',
                    message: 'Невалидно ЕГН!'
                },
                notEmpty: {
                    message: 'Това поле не може да бъде празно!'
                }
            }
        },
        id: {
            validators: {
                stringLength: {
                    min: 9,
                    max: 9,
                    message:'Номерът на лична карта съдържа 9 цифри!'
                },
                notEmpty: {
                    message: 'Това поле не може да бъде празно!'
                }
            }
        },
        adres: {
            validators: {
                notEmpty: {
                    message: 'Моля въведете адрес!'
                }
            }
        },
        mvr: {
            validators: {
                notEmpty: {
                    message: 'Това поле не може да бъде празно!'
                }
            }
        },
        region: {
            validators: {
                notEmpty: {
                    message: 'Моля изберете Област!'
                }
            },
        },
        city: {
            validators: {
                notEmpty: {
                    message: 'Моля изберете Град!'
                }
            }
        }
    }
    });

    $('#rootwizard').bootstrapWizard(
        {
            onTabClick: function(tab, navigation, index) {
                var invalid = $('#form_validate').bootstrapValidator('validate').has('.has-error').length;
                if(invalid){
                    $('#form_validate').bootstrapValidator('validate');
                    return false;
                }
            },
            onNext: function(tab, navigation, index) {
            var invalid = $('#form_validate').bootstrapValidator('validate').has('.has-error').length;
            if(invalid){
                $('#form_validate').bootstrapValidator('validate');
                return false;
             }
        },
        onTabShow: function(tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        $('#rootwizard').find('.progress-bar').css({width:$percent+'%'});
    }});

});
function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}

function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function isNumericKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return true;
    return false;
}


function region_city(event, idRegion, idCity) {
    var region_id = document.getElementById(idRegion);
    $.ajax({
        method: "POST",
        url: region,
        data:{reg_id:region_id.value, _token: token}
    })
    .done(function (msg) {
        var value = msg['message'];
        var cityId = document.getElementById(idCity);

        $(cityId).empty();
        if(region_id.value == '')
            $(cityId).append($('<option>').text('Изберете първо област ...').attr('value',''));
        else
            $(cityId).append($('<option>').text('Изберете ...').attr('value',''));

        value.forEach(function(obj){
            $(cityId).append($('<option>').text(obj['name']).attr('value',obj['id']));
            console.log(cityId);
        })
    })
}

function num2txt(idNum, idLabel) {
    var input = document.getElementById(idNum);
    $.ajax({
        method: "POST",
        url: convert2txt,
        data:{input_num:input.value, _token: token}
    })
        .done(function (msg) {
            var value = msg['message'];
            var label = document.getElementById(idLabel);

            //$(label).empty();
            $(label).text(value);
            console.log(value);
        })
}