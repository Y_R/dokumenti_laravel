<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentCategory extends Model
{
    public function document()
    {
        return $this->hasMany('App\Document');
    }
}
