<?php

namespace App\Http\Controllers;

use App\DocumentCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Document extends Controller
{
    public function getDocuments(){
        $documents = \App\Document::all();
        $documentCategoryAll = DocumentCategory::all();

        return view('welcome',['documents' => $documents, 'documentCategory' =>$documentCategoryAll]);
    }
    
    public function getCategories(){
        $allCat = DocumentCategory::all();
        return view('admin.document.add-document',['documentCategory' =>$allCat]);
    }

    public function addDocument(Request $request)
    {
        $this->validate($request,[
            'description' => 'required|max:1000',
            'document_name' => 'required'
        ]);

        $documentCategory = DocumentCategory::where('id',$request['role_id'])->first();
        $document = new \App\Document();
        $document->name = $request['document_name'];
        $document->description = $request['description'];

        $documentCategory->document()->save($document);

        return redirect()->route('addDocument');

    }

    public function addDocumentCategory(Request $request){
        $this->validate($request,[
            'document_category' => 'required'
        ]);

        $documentCategory = new DocumentCategory();
        $documentCategory->name = $request['document_category'];
        $documentCategory->save();
        return redirect()->route('addDocument');
    }
    
}
