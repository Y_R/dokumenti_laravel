<?php
/**
 * Created by PhpStorm.`
 * User: y.rusanov
 * Date: 19.01.2018
 * Time: 16:37
 */


namespace App\Http\Controllers;

use App\City;
use App\mvra;
use App\Region;
use App\tools\NumTxt;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GenerateDocumentController extends Controller
{

    public function getCity(Request $request){
        $id = $request['reg_id'];
        //$city = \App\Document::all();
        //$reg_id = Input::get('reg_id');
        $city = City::select('id','name')->where('region_id', $id)->get();

        //return view('generate.dogovor_naem',['city' => $city]);
        //return response()->json($city);
        //return response()->json(['success'=>'Got Simple Ajax Request.']);
        //return response()->json($city);

        //$data = City::all();
        return response()->json(['message' =>$city]);
//        return response([$reg_id]);
    }

    public function num2txt(){
        return response()->json(['message' =>'text']);
    }


    public function getRegion(){
        //$city = \App\Document::all();
        $region = Region::all();
        $mvra = mvra::all();
        return view('generate.dogovor_naem',['region' => $region, 'mvr' => $mvra]);
    }

    public function generate(){
        return view('document.documents');
    }
    
    public function convert2txt(Request $request){
        $text = $request['input_num'];
        
        $nn = new NumTxt();
        $nn->setNum($text);
        $nn->generate();
        $num = $nn->getNum();
        
        return response()->json(['message' => $num]);
    }
}