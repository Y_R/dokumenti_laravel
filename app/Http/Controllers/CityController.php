<?php
/**
 * Created by PhpStorm.
 * User: y.rusanov
 * Date: 19.01.2018
 * Time: 16:23
 */

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class CityController extends Controller
{
    public function getCity(){
        //$city = \App\Document::all();
        $city = City::all();
        return view('generate.dogovor_naem',['city' => $city]);
    }
}