<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    public function documentCategory()
    {
        return $this->belongsTo('App\DocumentCategory');
    }
}
