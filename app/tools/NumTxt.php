<?php
namespace App\tools;
use Symfony\Thanks\Thanks;

    /**
 * Created by PhpStorm.
 * User: y.rusanov
 * Date: 02.03.2018
 * Time: 17:43
 */

/* algo for num 2 txt
142

142 % 1000   0 = сто
42 % 100 = 0 = 4 десет
2 % 10 = 0 2 единици



3564 % 10000 = 0 3 хиляди
564 % 1000 = 0 5 сто
64 % 100 = 0 6 десет
4 % 10 = 0 единци

65 % 100 = 0 6 десет
5 % 10 = 0 5 единици


int position = 0;
int number = 10;
real whole;
real value;

do{
	if(value % (number^position) == 0)
	{
		print ( (value / (number^(position-1)) , (number^(position-1)) );
		whole = floor(value);
		value = value - whole;
	}
	position ++;
}
while(true)

*/


class NumTxt
{
    private $value;
    private $tenth_tweny = false;
    private $number = 10;
    private $position = 0;
    private $whole;
    private $txt;
    private $ones = array(
    "един","двe", "три", "четири", "пет", "шест", "седем", "осем", "девет"
    );

    private $twenty = array(
        "единадесет",
        "дванадесет",
        "тринадесет",
        "четиринадесет",
        "петнадесет",
        "шестнадесет",
        "осемнадесет",
        "деветнадесет"
    );

    private $tenth = array(
        "десет","двадесет", "тридесет", "четиридесет", "петдесет", "шестдесет", "седемдесет", "осемдесет", "деветдесет"
    );

    private  $big = array(
        "сто", "хиляда", "милион", "милиард"
    );

    private  $hundred = array(
        "сто", "ста", "стотин"
    );

    public function setNum($num){
        $this->value = $num;
    }

    public function getNum(){
        return $this->value;
    }

    private function convertToTxt($printNum, $numZeros, $tenth_){
        
    }

    private function getOnes($num){
        if($num == 2){
            return "Два";
        }
        return $this->ones[$num - 1];
    }

    private function getHundred($num){
        if($num > 1 && $num < 4)
            return $this->getOnes($num) . $this->hundred[1];
        else if($num > 3)
            return $this->getOnes($num) . $this->hundred[2];
        else
            $this->hundred[0];
        return 1;
    }

    private function getTenth($num){
        return $this->tenth[$num - 1];
    }

    public function generate(){
        $this->position=0;
        $localpos = 0;
        do{
            $this->number = pow(10 , $this->position);
            $newValue1 = $this->value / $this->number;
            $this->whole = floor($newValue1);

            if($this->whole == 0){
                $printNum = floor($this->value / pow(10, ($this->position - 1)));
                if($printNum == 0)
                    break;

                $modNum = fmod($this->value , pow(10, ($this->position - 1)));
                $numZeros = pow(10, ($this->position - 1));
                if($numZeros == 10 && $printNum == 1)
                    $this->convertToTxt($printNum, $numZeros, true);
                else
                    $this->convertToTxt($printNum, $numZeros, false);
                //$this->txt .= "<".++$localpos.">" . "Print: " . $printNum . " | " . "Mod: ". $modNum . " | " . "Position: " . $numZeros . "</>";
                $this->value = $modNum;
                $this->position =0;
                //$this->whole = -1;
            }

            //$this->txt .= "Value: ".$newValue1. " | " .  "Position: ".$this->position. " | ". "Number: " . $this->number . " | ";
            $this->position++;
            //if($this->whole==0)
              //  break;
        }while(true);

        $this->value = $this->txt;
    }
}