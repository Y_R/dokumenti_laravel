<div class="form-row">
    <div class="form-group">
        <label class="control-label">Изберете вид на Заемодателя</label>
        <select class="form-control m-bot15" name="role_id" id="role_id">
            <option value="1">ФИЗИЧЕСКО ЛИЦЕ</option>
            <option value="1">ЮРИДИЧЕСКО ЛИЦЕ</option>
        </select>
    </div>
    <div class="row">
        <div class="form-row">
            <div class="form-group col-md-3">
                <label class="control-label">Име</label>
                <input class="form-control text-uppercase" type="text" onkeypress="return isNumericKey(event)" name="firstname" id="firstname" >
            </div>

            <div class="form-group col-md-3">
                <label class="control-label">Презиме</label>
                <input class="form-control text-uppercase" type="text" onkeypress="return isNumericKey(event)" name="surename" id="surename" >
            </div>

            <div class="form-group col-md-3">
                <label class="control-label">Фамилия</label>
                <input class="form-control text-uppercase" type="text" onkeypress="return isNumericKey(event)" name="lastname" id="lastname" >
            </div>

            <div class="form-group col-md-3">
                <label class="control-label">ЕГН</label>
                <input class="form-control" type="text" name="egn" onkeypress="return isNumberKey(event)" id="egn" placeholder="ЕГН">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-row">
            <div class="form-group col-md-4">
                <label class="control-label">Лична карта</label>
                <input class="form-control" type="text" name="id" onkeypress="return isNumberKey(event)" maxlength="9" id="id" placeholder="Лична карта">
            </div>
            @include('generate.moduls.datepicker')
        </div>
        <div class="row">
            <div class="form-group col-md-4 ">
                <label class="control-label">МВР</label>
                <select class="form-control m-bot15" name="mvr" id="mvr">
                    <option value="">Изберете ...</option>
                    @foreach($mvr as $mvrs)
                        <option value="{{$mvrs->id}}">{{$mvrs->name}}</option>
                        @endForeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-row">

            @include('generate.moduls.region_city')

            <div class="form-group col-md-4">
                <label class="control-label">Адрес</label>
                <input class="form-control text-uppercase" type="text" name="adres" id="adres" placeholder="Адрес">
            </div>
        </div>
    </div>
</div>