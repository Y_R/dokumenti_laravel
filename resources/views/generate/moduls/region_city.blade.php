
@php
    $idRegion = "region_".$id;
    $idCity = "city_".$id
@endphp

<div class="form-group col-md-4">
    <label class="control-label">Регион</label>
    <select class="form-control" onchange="region_city(event,'{{$idRegion}}', '{{$idCity}}')" name="region" id="{{$idRegion}}" >
        <option value="">Изберете ...</option>
        @foreach($region as $regions)
            <option value="{{$regions->id}}">{{$regions->name}}</option>
            @endForeach

    </select>
</div>

<div class="form-group col-md-4">
    <label class="control-label">Град</label>
    <select class="form-control m-bot15" name="city" id="{{$idCity}}">
        <option value="">Изберете първо област ...</option>
    </select>
</div>

