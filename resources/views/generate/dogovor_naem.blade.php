@extends('master.master')

@section('content')
    <div id="rootwizard">
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <ul>
                        <li><a href="#tab1" data-toggle="tab">First</a></li>
                        <li><a href="#tab2" data-toggle="tab">Second</a></li>
                        <li><a href="#tab3" data-toggle="tab">Third</a></li>
                        <li><a href="#tab4" data-toggle="tab">Forth</a></li>
                        <li><a href="#tab5" data-toggle="tab">Fifth</a></li>
                        <li><a href="#tab6" data-toggle="tab">Sixth</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="bar" class="progress progress-striped active">
            <div class="progress-bar"></div>
        </div>
        <div class="tab-content" id="form_validate">
            <div class="tab-pane" id="tab1">
                <h1 class="text-center">Данни на Заемодателя</h1>
                @php
                    $dateLabel = 'Издадена на';
                    $id = '1';
                @endphp
                @include('generate.moduls.person')
            </div>
            <div class="tab-pane" id="tab2">
                <h1 class="text-center">Заемополучателя</h1>
                @php
                    $dateLabel = 'Издадена на';
                    $id = '2';
                @endphp
                @include('generate.moduls.person')
            </div>
            <div class="tab-pane" id="tab3">
                <h1 class="text-center">Дата на предоставяне на заема</h1>
                @php
                    $dateLabel = '';
                    $id = '3';
                @endphp

                <div class="row">
                    <div class="form-group">
                        <h4 class="sub_title">Дата на предоставяне на заема</h4>
                        @include('generate.moduls.datepicker')
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <h4 class="sub_title">Град на сключване на договора</h4>
                        @include('generate.moduls.region_city')
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <h4 class="sub_title">Начин на предоставяне на заема</h4>
                        <div class="form-group col-md-4">
                            <select class="form-control" name="pay" id="pay">
                                <option value="1">В БРОЙ</option>
                                <option value="1">ПО БАНКОВ ПЪТ</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div class="tab-pane" id="tab4">
                <h1 class="text-center">Дата на връщане на заема</h1>
                <div class="row">
                    <div class="form-group">
                        <h4 class="sub_title">Дата на връщане на заема</h4>
                        @include('generate.moduls.datepicker')
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <h4 class="sub_title">Начин на предоставяне на заема</h4>
                        <div class="form-group col-md-4">
                            <select class="form-control" name="pay" id="pay">
                                <option value="1">В БРОЙ</option>
                                <option value="1">ПО БАНКОВ ПЪТ</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab5">
                <h1 class="text-center">Сума и лихва на заема</h1>
                <div class="row">
                    <div class="form-group">
                        <h4 class="sub_title">Валута на заема</h4>
                        <div class="form-group col-md-4">
                            <select class="form-control" name="pay" id="pay">
                                <option value="1">ЛЕВА</option>
                                <option value="1">ЕВРО</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <h4 class="sub_title">Сума на заема (в Лева)</h4>
                    <div class="form-group col-md-4">
                        <input class="form-control" type="text" onkeypress="return isNumberKey(event)">
                    </div>
                </div>

                <div class="row">
                    <h4 class="sub_title">Годишна лихва (в %)</h4>
                    <div class="form-group col-md-4">
                        <input class="form-control" type="text" onkeypress="return isNumberKey(event)">
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab6">
                Final
            </div>
            <ul class="pager wizard">
                <li class="previous"><a href="javascript:;">Previous</a></li>
                <li class="next"><a href="javascript:;">Next</a></li>
            </ul>
        </div>
    </div>

    <div class="row">
        <h4 class="sub_title">Сума на заема (в Лева)</h4>
        <div class="form-group col-md-4">
            <input class="form-control" type="text" id="inputNum2txt"
                   onkeypress="return isNumberKey(event)"
                   onchange="num2txt('inputNum2txt','labelNum2txt')"/>
            <label class="control-label" id="labelNum2txt">asdsadadasdasdsa</label>
        </div>
    </div>

@endsection

<script>
    var region = '{{route('ajax')}}';
    var convert2txt = '{{route('convert2txt')}}';
    var token = '{{Session::token()}}';
</script>

