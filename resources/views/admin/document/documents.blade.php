@extends('master.master')
@section('content')
    <section class="row posts">
        <div class="col-md-6 col-md-offset-3">
            @foreach($documentCategory as $category)
                <article class="post">
                    <p>{{$category->name}}</p>
                    @foreach($documents as $document)
                        @if($document->documentCategory->id == $category->id)
                            <article >
                                <a href="{{route('generate.dogovor_naem')}}">{{$document->name}}</a>
                            </article>
                        @endif
                    @endforeach
                </article>
            @endforeach
        </div>
    </section>
    <a href="{{route('addDocument')}}">Add document</a>
@endsection