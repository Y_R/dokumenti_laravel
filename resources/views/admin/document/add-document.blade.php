@extends('master.master')

<section class="row new-post">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <form action="{{route('document.new')}}" method="post">

                    <div class="form-group">
                        <label for="document_name">Document Name</label>
                        <input class="form-control" type="text" name="document_name" id="document_name">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="description" id="description" rows="5" placeholder="Your post"></textarea>
                    </div>
                    <div class="col-md-6 col-md-offset-3">
                        <select class="form-control m-bot15" name="role_id" id="role_id">
                            @foreach($documentCategory as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endForeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Add document</button>
                    <input type="hidden" value="{{Session::token()}}" name="_token">
                </form>
            </div>

            <div class="col-md-6 col-md-offset-3">
                <form action="{{route('document.newCategory')}}" method="post">
                    <div class="form-group">
                        <label for="document_category">Document Category</label>
                        <input class="form-control" type="text" name="document_category" id="document_category">
                    </div>
                    <button type="submit" class="btn btn-primary">Add document category</button>
                    <input type="hidden" value="{{Session::token()}}" name="_token">
                </form>
            </div>

        </div>
    </section>