<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{URL::to('src/css/main.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::to('src/css/style.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::to('src/css/generate_document.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::to('src/css/generate_document_v1.css')}}" type="text/css">

    <!--js jquery-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!--js jquery-->

    <script src="{{URL::to('src/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::to('src/bootstrap/js/jquery.bootstrap.wizard.js')}}"></script>
    <script src="{{URL::to('src/js/num2text.js')}}"></script>


    <!--js bootstrap, bootstra validate-->
    <script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>
    <!--js bootstrap, bootstra validate-->


    <!--js custom-->
    <script src="{{URL::to('src/js/generate.js')}}"></script>

    <!--js custom-->

</head>
<body>

<div class="container">
    @yield('content')
</div>

</body>
</html>