<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('regions')->insert(['name' => 'Благоевград']);
        DB::table('regions')->insert(['name' => 'Бургас']);
        DB::table('regions')->insert(['name' => 'Варна']);
        DB::table('regions')->insert(['name' => 'Велико Търново']);
        DB::table('regions')->insert(['name' => 'Видин']);
        DB::table('regions')->insert(['name' => 'Враца']);
        DB::table('regions')->insert(['name' => 'Габрово']);
        DB::table('regions')->insert(['name' => 'Добрич']);
        DB::table('regions')->insert(['name' => 'Кърджали']);
        DB::table('regions')->insert(['name' => 'Кюстендил']);
        DB::table('regions')->insert(['name' => 'Ловеч']);
        DB::table('regions')->insert(['name' => 'Монтана']);
        DB::table('regions')->insert(['name' => 'Пазарджик']);
        DB::table('regions')->insert(['name' => 'Перник']);
        DB::table('regions')->insert(['name' => 'Плевен']);
        DB::table('regions')->insert(['name' => 'Пловдив']);
        DB::table('regions')->insert(['name' => 'Разград']);
        DB::table('regions')->insert(['name' => 'Русе']);
        DB::table('regions')->insert(['name' => 'Силистра']);
        DB::table('regions')->insert(['name' => 'Сливен']);
        DB::table('regions')->insert(['name' => 'Смолян']);
        DB::table('regions')->insert(['name' => 'София (столица)']);
        DB::table('regions')->insert(['name' => 'София']);
        DB::table('regions')->insert(['name' => 'Стара Загора']);
        DB::table('regions')->insert(['name' => 'Търговище']);
        DB::table('regions')->insert(['name' => 'Хасково']);
        DB::table('regions')->insert(['name' => 'Шумен']);
        DB::table('regions')->insert(['name' => 'Ямбол']);



        $Region = DB::table('regions')->where('name' , 'Благоевград')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Банско', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Белица', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Благоевград', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Гоце Делчев', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Гърмен', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Кресна', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Петрич', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Разлог', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Сандански', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Сатовча', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Симитли', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Струмяни', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Хаджидимово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Якоруда', 'region_id' => $RegionId]);


        $Region = DB::table('regions')->where('name' , 'Бургас')->first();
        $RegionId = $Region->id;
        //Бургас
        DB::table('cities')->insert(['name' => 'Айтос', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Бургас', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Камено', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Карнобат', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Малко Търново', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Несебър', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Поморие', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Приморско', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Руен', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Созопол', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Средец', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Сунгурларе', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Царево', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Варна')->first();
        $RegionId = $Region->id;
        //Варна
        DB::table('cities')->insert(['name' => 'Аврен', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Аксаково', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Белослав', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Бяла', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Варна', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Ветрино', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Вълчи дол', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Девня', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Долни чифлик', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Дългопол', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Провадия', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Суворово', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Велико Търново')->first();
        $RegionId = $Region->id;
        //Велико Търново
        DB::table('cities')->insert(['name' => 'Велико Търново', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Горна Оряховица', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Елена', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Златарица', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Лясковец', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Павликени', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Полски Тръмбеш', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Свищов', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Стражица', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Сухиндол', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Видин')->first();
        $RegionId = $Region->id;
        //Видин
        DB::table('cities')->insert(['name' => 'Белоградчик', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Бойница', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Брегово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Видин', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Грамада', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Димово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Кула', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Макреш', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Ново село', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'ужинци', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Чупрене', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Враца')->first();
        $RegionId = $Region->id;
        //Враца
        DB::table('cities')->insert(['name' => 'Борован', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Бяла Слатина', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Враца', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Козлодуй', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Криводол', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Мездра', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Мизия', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Оряхово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Роман', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Хайредин', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Габрово')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Габрово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Дряново', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Севлиево', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Трявна', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Добрич')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Балчик', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Генерал Тошево', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Добрич', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Добрич - град', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Каварна', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Крушари', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Тервел', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Шабла', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Кърджали')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Ардино', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Джебел', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Кирково', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Крумовград', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Кърджали', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Момчилград', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Черноочене', 'region_id' => $RegionId]);


        $Region = DB::table('regions')->where('name' , 'Кюстендил')->first();
        $RegionId = $Region->id;
        //Кюстендил
        DB::table('cities')->insert(['name' => 'Бобов дол', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Бобошево', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Дупница', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Кочериново', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Кюстендил', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Невестино', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Рила', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Сапарева баня', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Трекляно', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Ловеч')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Априлци', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Летница', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Ловеч', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Луковит', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Тетевен', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Троян', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Угърчин', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Ябланица', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Монтана')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Берковица', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Бойчиновци', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Брусарци', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Вълчедръм', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Вършец', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Георги Дамяново', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Лом', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Медковец', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Монтана', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Чипровци', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Якимово', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Пазарджик')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Батак', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Белово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Брацигово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Велинград', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Лесичово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Пазарджик', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Панагюрище', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Пещера', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Ракитово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Септември', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Стрелча', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Сърница', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Перник')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Брезник', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Земен', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Ковачевци', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Перник', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Радомир', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Трън', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Плевен')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Белене', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Гулянци', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Долна Митрополия', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Долни Дъбник', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Искър', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Кнежа', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Левски', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Никопол', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Плевен', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Пордим', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Червен бряг', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Пловдив')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Асеновград', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Брезово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Калояново', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Карлово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Кричим', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Куклен', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Лъки', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Марица', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Перущица', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Пловдив', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Първомай', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Раковски', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Родопи', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Садово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Сопот', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Стамболийски', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Съединение', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Хисаря', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Разград')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Завет', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Исперих', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Кубрат', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Лозница', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Разград', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Самуил', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Цар Калоян', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Русе')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Борово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Бяла', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Ветово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Две могили', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Иваново', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Русе', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Сливо поле', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Ценово', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Силистра')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Алфатар', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Главиница', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Дулово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Кайнарджа', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Силистра', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Ситово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Тутракан', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Сливен')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Котел', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Нова Загора', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Сливен', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Твърдица', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Смолян')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Баните', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Борино', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Девин', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Доспат', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Златоград', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Мадан', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Неделино', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Рудозем', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Смолян', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Чепеларе', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'София (столица)')->first();
        $RegionId = $Region->id;
        //София (столица)
        DB::table('cities')->insert(['name' => 'Столична', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'София')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Антон', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Божурище', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Ботевград', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Годеч', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Горна Малина', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Долна баня', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Драгоман', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Елин Пелин', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Етрополе', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Златица', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Ихтиман', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Копривщица', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Костенец', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Костинброд', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Мирково', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Пирдоп', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Правец', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Самоков', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Своге', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Сливница', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Чавдар', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Челопеч', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Стара Загора')->first();
        $RegionId = $Region->id;
        DB::table('cities')->insert(['name' => 'Братя Даскалови', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Гурково', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Гълъбово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Казанлък', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Мъглиж', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Николаево', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Опан', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Павел баня', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Раднево', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Стара Загора', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Чирпан', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Търговище')->first();
        $RegionId = $Region->id;
        //Търговище
        DB::table('cities')->insert(['name' => 'Антоново', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Омуртаг', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Опака', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Попово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Търговище', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Хасково')->first();
        $RegionId = $Region->id;
        //Хасково
        DB::table('cities')->insert(['name' => 'Димитровград', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Ивайловград', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Любимец', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Маджарово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Минерални бани', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Свиленград', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Симеоновград', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Стамболово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Тополовград', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Харманли', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Хасково', 'region_id' => $RegionId]);

        $Region = DB::table('regions')->where('name' , 'Шумен')->first();
        $RegionId = $Region->id;
        //Шумен
        DB::table('cities')->insert(['name' => 'Велики Преслав', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Венец', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Върбица', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Каолиново', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Каспичан', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Никола Козлево', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Нови пазар', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Смядово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Хитрино', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Шумен', 'region_id' => $RegionId]);
        
        $Region = DB::table('regions')->where('name' , 'Ямбол')->first();
        $RegionId = $Region->id;
        //Ямбол
        DB::table('cities')->insert(['name' => 'Болярово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Елхово', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Стралджа', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Тунджа', 'region_id' => $RegionId]);
        DB::table('cities')->insert(['name' => 'Ямбол', 'region_id' => $RegionId]);

    }
}
