<?php

use Illuminate\Database\Seeder;

class mvr extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mvras')->insert(['name' => 'МВР - Благоевград']);
        DB::table('mvras')->insert(['name' => 'МВР - Бургас']);
        DB::table('mvras')->insert(['name' => 'МВР - Варна']);
        DB::table('mvras')->insert(['name' => 'МВР - Велико Търново']);
        DB::table('mvras')->insert(['name' => 'МВР - Видин']);
        DB::table('mvras')->insert(['name' => 'МВР - Враца']);
        DB::table('mvras')->insert(['name' => 'Габрово - МВР']);
        DB::table('mvras')->insert(['name' => 'ДНСП']);
        DB::table('mvras')->insert(['name' => 'МВР - Добрич']);
        DB::table('mvras')->insert(['name' => 'МВР - Кърджали']);
        DB::table('mvras')->insert(['name' => 'МВР - Кюстендил']);
        DB::table('mvras')->insert(['name' => 'МВР - Ловеч']);
        DB::table('mvras')->insert(['name' => 'МВР - Монтана']);
        DB::table('mvras')->insert(['name' => 'МВР - Пазарджик']);
        DB::table('mvras')->insert(['name' => 'МВР - Перник']);
        DB::table('mvras')->insert(['name' => 'МВР - Плевен']);
        DB::table('mvras')->insert(['name' => 'МВР - Пловдив']);
        DB::table('mvras')->insert(['name' => 'МВР - Разград']);
        DB::table('mvras')->insert(['name' => 'МВР - Русе']);
        DB::table('mvras')->insert(['name' => 'МВР - Силистра']);
        DB::table('mvras')->insert(['name' => 'МВР - Сливен']);
        DB::table('mvras')->insert(['name' => 'МВР - Смолян']);
        DB::table('mvras')->insert(['name' => 'МВР - София']);
        DB::table('mvras')->insert(['name' => 'МВР - София област']);
        DB::table('mvras')->insert(['name' => 'МВР - Стара Загора']);
        DB::table('mvras')->insert(['name' => 'МВР - Търговище']);
        DB::table('mvras')->insert(['name' => 'МВР - Хасково']);
        DB::table('mvras')->insert(['name' => 'МВР - Шумен']);
    }
}
